package entity;

public class Admin {
	private int adminid;
	private String username;
	private String password;
	private String realname;
	
	@Override
	public String toString() {
		return "Admin [adminid=" + adminid + ", username=" + username
				+ ", password=" + password + ", realname=" + realname + "]";
	}
	public Admin() {
		super();
	}
	public Admin(String username, String password, String realname) {
		super();
		this.username = username;
		this.password = password;
		this.realname = realname;
	}
	public int getAdminid() {
		return adminid;
	}
	public void setAdminid(int adminid) {
		this.adminid = adminid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}

}
