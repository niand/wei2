package dao;
import entity.Admin;
public interface AdminDao {
	
    public int insertAdmin(Admin admin);
    public Admin selectByUsername(String username);
}
