package test;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import com.gsdxsys.dao.impl.MyBatisUtils;
import com.gsdxsys.entity.Admin;

public class MybatisTest {

	@Test
	public void testSelect() {

		
		SqlSession sqlSession = null;
		SqlSessionFactory sqlSessionFactory = null;

		try {
			sqlSessionFactory = MyBatisUtils.createFactory();
			sqlSession = sqlSessionFactory.openSession();
			List<Admin> admins = sqlSession.selectList("Admin.selectAll");
			for (Admin admin : admins) {
				System.out.println(admin.getAdminid() + ":" + admin.getRealname());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (sqlSession != null) {
				sqlSession.close();
			}
		}

	}

	@Test
	public void testInsert() {

		
		SqlSession sqlSession = null;
		
		try {
			
			SqlSessionFactory sqlSessionFactory = MyBatisUtils.createFactory();
			sqlSession = sqlSessionFactory.openSession();
			Admin admin = new Admin();
			admin.setUsername("xiaoshiyi");
			admin.setPassword("123456");
			admin.setRealname("ʮһ");
			int result = sqlSession.insert("Admin.insertAdmin", admin);
			sqlSession.commit();
			System.out.println(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (sqlSession != null) {
				sqlSession.close();
			}
		}
	}

	@Test
	public void testDel() {

		
		SqlSession sqlSession = null;
	
		try {
			
			SqlSessionFactory sqlSessionFactory = MyBatisUtils.createFactory();
			sqlSession = sqlSessionFactory.openSession();

			int result = sqlSession.insert("Admin.delById", 27);
			sqlSession.commit();
			System.out.println(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (sqlSession != null) {
				sqlSession.close();
			}
		}
	}

	@Test
	public void testEdit() {

	
		SqlSession sqlSession = null;
		
		try {
		
			SqlSessionFactory sqlSessionFactory = MyBatisUtils.createFactory();
			sqlSession = sqlSessionFactory.openSession();
			Admin admin = new Admin();
			admin.setAdminid(1);
			admin.setUsername("xiaozhang");
			admin.setPassword("123456");
			admin.setRealname("С");

			int result = sqlSession.insert("Admin.updateAdmin", admin);
			sqlSession.commit();
			System.out.println(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (sqlSession != null) {
				sqlSession.close();
			}
		}
	}

}
