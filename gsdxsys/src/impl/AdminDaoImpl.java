package impl;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.AdminDao;
import entity.Admin;

public class AdminDaoImpl extends BaseDao implements AdminDao {
	public int insertAdmin(Admin admin){
		int result=0;
	    String sql="insert into admin(username,password,realname)values(?,?,?)";
	    Object[] values= {admin.getUsername(),admin.getPassword(),admin.getRealname()};
	    Connection conn=DbUtils.createConn();
	    super.executeUpdate(conn,sql,values);
	    try {
	    	conn.close();
	    }catch(SQLException e){
	    	e.printStackTrace();
	    }	    
		return result;		
	}
	
		
	public Admin selectByUsername(String username){
		Admin admin=null;	
		String sql="select * from admin where username=?";
		Object[] values= {username};
		Connection conn=DbUtils.createConn();
		ResultSet rs=super.executeQuery(conn,sql,values);
		try {
		if(rs.next()) {
			admin=new Admin();
			admin.setAdminid(rs.getInt("adminid"));
			admin.setPassword(rs.getString("password"));
			admin.setRealname(rs.getString("realname"));
			admin.setUsername(rs.getString("username"));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		
		}
		finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
		return admin;
	}

}
