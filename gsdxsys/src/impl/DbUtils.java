package impl;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

public class DbUtils {
	public static Connection createConn(){
		
	Connection conn=null;
	String url="jdbc:mysql://localhost:3306/softdb";
	String uid="root";
	String pwd="123456";
	String clsname="com.mysql.jdbc.Driver";	
	try {
		Class.forName(clsname);
	}catch(ClassNotFoundException e) {
		e.printStackTrace();
	}
	try {
		conn=DriverManager.getConnection(url,uid,pwd);
	}catch(SQLException e) {
		e.printStackTrace();
	}
	return conn;
	}

}
