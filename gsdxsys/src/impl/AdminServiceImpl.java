package impl;
import dao.AdminDao;
import impl.AdminDaoImpl;
import entity.Admin;
import service.AdminService;
public class AdminServiceImpl implements AdminService {
	private AdminDao admindao=new AdminDaoImpl();	
	public boolean login(Admin admin) {
		boolean result=false;
		Admin realadmin=admindao.selectByUsername(admin.getUsername());
		if(realadmin!=null) {
			if(realadmin.getPassword().equals(admin.getPassword())) {
				result=true;
			}
		}
		return result;
	}
	public int reg(Admin admin) {
		int result=0;
		Admin realadmin=admindao.selectByUsername(admin.getUsername());
		if(realadmin==null) {
			admindao.insertAdmin(admin);
		}else {
			result=-1;
		}		
		return result;
	}
}

