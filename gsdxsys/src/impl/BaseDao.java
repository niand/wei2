package impl;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class BaseDao {
	public int executeUpdate(Connection conn,String sql,Object[] values) {
		int result=0;
		PreparedStatement pstms=null;
		try {
		     pstms=conn.prepareStatement(sql);
		     if(values!=null) {
		    	 for(int i=0;i<values.length;i++) {
		    		 pstms.setObject(i+1, values[i]);
		    	 }
		     }
		     result=pstms.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			
		}
		return result;
	}
	public ResultSet executeQuery(Connection conn,String sql,Object[] values) {
		ResultSet rs=null;
		PreparedStatement pstms=null;
		try {
		     pstms=conn.prepareStatement(sql);
		     if(values!=null) {
		    	 for(int i=0;i<values.length;i++) {
		    		 pstms.setObject(i+1, values[i]);
		    	 }
		     }
		     rs=pstms.executeQuery();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			
		}
		return rs;
	}

}
